<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>HELLO</title>
</head>
<body>
<h4 id = "h">Welcome Page</h4>
<div class="container" ng-app="EmpReim">
    <div ng-controller="ProductHomeController">
        <p id = "myPara">ASDF</p>
    </div>
</div>


<script>
    var app = angular.module("EmpReim", []);

    app.controller("ProductHomeController", function ($http) {
        var promise = $http({
            method: "GET",
            url: "/login"
//            url: "/er/fc/login"
        });

        promise.then(function (response) {
            console.log(response);
            document.getElementById("myPara").innerHTML = response.data;
        }, function (error) {
            console.error(error.data);
        });
    });
</script>
</body>
</html>
