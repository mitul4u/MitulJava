<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO-8859-1">
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <title>Employee Reimbursement System</title>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
</head>
<style>
    body {
        background: #ccc url("/Images/backgroundImage.jpg");
        font-family: 'Roboto', serif;
        font-weight: 100;
        line-height: 1.2em;
        margin: 50px auto;
        color: #ccc;
    }

    h1 {
        font-size: 200px;
        /*opacity: .5;*/
        position: relative;
        left: -20px;
        top: -90px;
        float: left;
    }

    .container {
        width: 400px;
        height: 350px;
        background-color: #31330f;
        margin: 30px auto;
        padding: 30px;
        border: 1px solid #999;
        box-shadow: 0 0 10px #333;
    }

    ul {
        margin: 0;
        padding: 0;
        list-style: none;
        font-size: 24px;
        color: #fff;
        position: relative;
        bottom: 70px;
    }

    input {
        padding: 4px;
        margin: 8px 0 15px;
        font-family: 'Roboto', serif;
    }

    input:focus {
        border: none;
        background-color: #664945;
        outline: none;
        padding: 6px;
        color: #fff;
        font-family: 'Roboto', serif;
    }

    button {
        border-radius: 7px;
        background-color: #999;
        border: none;
        padding: 6px 15px;
        color: #fff;
        margin: 0;
        font-family: 'Roboto', serif;
    }

    button:hover {
        background-color: #666;
        border: none;
        padding: 6px 15px;
        color: #fff;
        margin: 0;
        font-family: 'Roboto', serif;
    }
</style>
<%--<body ng-app="ERS">--%>
<%--&lt;%&ndash;//Testing Angular Stuff&ndash;%&gt;--%>
<%--<div ng-controller = "LoginController">--%>
    <%--<h5>ERS</h5>--%>
    <%--<input type="text" ng-model="user" id="user" value="userName" name="userid"><br>--%>
    <%--<input type="text" ng-model="pass" id="pass" value="passWord" name="pass"><br>--%>
    <%--<button ng-click="checkLogin(user,pass)" >Submit</button>--%>

    <%--<div class="container">--%>
        <%--<div class="alert alert-danger" style="display:none;" id="loginError">--%>
            <%--<strong>Incorrect username or password</strong>--%>
        <%--</div>--%>

        <%--<div style="display:none;" id="successLogin">--%>
            <%--<strong>Loading profile....</strong>--%>
        <%--</div>--%>
    <%--</div>--%>
<%--</div>--%>
<%--<script type="text/javascript" src="Javascript/app.js"></script>--%>
<%--&lt;%&ndash;<script type="text/javascript" src="Javascript/login.js"></script>&ndash;%&gt;--%>
<%--</body>--%>
<%--</html>--%>

<body>
<%--Html & JSTL--%>
<form action="login.do" method="post">
    <div class="container">
        <h1>ERS</h1>
        <ul>
            <li><c:if test="${not empty authFailed}">
                <span style="color: red; "> <c:out value="${authFailed}" />
                </span>
            </c:if></li>
            <li><label>Username</label></li>
            <li><input type="text" name="userid"></li>
            <li><label>Password</label></li>
            <li><input type="password" name="pass">
                <button type="submit">Sign In</button></li>
        </ul>
    </div>
</form>
<script type="text/javascript" src="Javascript/app.js"></script>
<script type="text/javascript" src="Javascript/login.js"></script>
</body>
</html>
