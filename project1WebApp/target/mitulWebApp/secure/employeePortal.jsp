<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Employee Reimbursements</title>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script>
	function myFunction() {
		// Declare variables
		var input, filter, table, tr, td, i;
		input = document.getElementById("myInput");
		filter = input.value.toUpperCase();
		table = document.getElementById("eTable");
		tr = table.getElementsByTagName("tr");

		// Loop through all table rows, and hide those who don't match the search query
		for (i = 0; i < tr.length; i++) {
			td = tr[i].getElementsByTagName("td")[6];
			td2 = tr[i].getElementsByTagName("td")[8];
			if (td) {
				if ( td.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1) {
					tr[i].style.display = "";
				} else {
					tr[i].style.display = "none";
				}
			}
		}
	}
</script>

</head>

<style>
html, body {
	background: #ccc url("/Images/backgroundImage.jpg");
}
table {
	background-color: whitesmoke;
}
#menuBar {
	background-color: black;
	opacity: .85;
}

#leftBar {
	color: white;
	font-size: 30px;
	opacity: .6;
}

#myInput {
	height: 2.5em;
	width: 100%;
	margin-top: .25em;
}

#logoutButton {
	background: transparent;
	border: none;
	font-size: 17px;
	float: right;
	color: #ccc;
	margin-top: .2em;
}

#reimbButton {
	float: right;
}

</style>

<body>
	<div id="employeeView" class="container">
		<div style="overflow: auto;">
				<div id="menuBar" class="row">
					<div id="leftBar" class="col-lg-3"></div>
					<!-- /.col-lg-3 -->
					<div id="centerBar" class="col-lg-6">
						<input type="text" id="myInput" onkeyup="myFunction()"
							   placeholder="Search for descriptions or status..">
					</div>
					<!-- /.col-lg-6 -->
					<div id="rightBar" class="col-lg-3">
						<button id="logoutButton" type="button"
								class="btn btn-default dropdown-toggle" data-toggle="dropdown"
								aria-haspopup="true" aria-expanded="false">
							Signed in as
							<c:out value="${name}" />
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu dropdown-menu-right">
							<li><a href="/ers/logOut.do">log out</a></li>
						</ul>
					</div>
					<!-- /.col-lg-6 -->
				</div>

			<table id="eTable" class="table table-striped" style="max-height: 500px;">
				<tr>
					<th>ID</th>
					<th>Submitted</th>
					<th>Status</th>
					<th>Amount</th>
					<th>Resolved Date</th>
					<th>Resolved Amount</th>
					<th>Manager</th>
					<th>Comments</th>
					<th>Type</th>
					<th>Receipt</th>
				</tr>

				<c:forEach var="reim" items="${REIMBList}">
					<tr>
						<td><c:out value="${reim.reimberId}" /></td>
						<td><c:out value="${reim.submitted}" /></td>
						<td><c:out value="${reim.status}" /></td>
						<td><fmt:formatNumber type="currency" value="${reim.reimbursementAmount}" /></td>
						<td><c:out value="${reim.resolvedDate}" /></td>
						<td><fmt:formatNumber type="currency" value="${reim.resolvedAmt}" /></td>
						<td><c:out value="${reim.managerFullName}" /></td>
						<td><c:out value="${reim.comments}" /></td>
						<td><c:out value="${reim.reimbType}" /></td>
						<td><c:out value="${reim.receipt}" /> </td>
					</tr>
				</c:forEach>
			</table>
		</div>
        <div>

            <!-- Trigger the Reimbursement with a button -->
            <button id="reimbButton" type="button" class="btn btn-info btn-sm"
                    data-toggle="modal" data-target="#myModel">New
                Reimbursement</button>

            <%--<br>--%>
            <%--<form action="updateEmployee.do" method="post">--%>
                <%--<tr>Update your Profile</tr>--%>
                <%--<tr>--%>
                    <%--<td>First Name</td>--%>
                    <%--<td><input type="text" name="fName"></td>--%>
                    <%--<td>Last Name</td>--%>
                    <%--<td><input type="text" name="lName"></td>--%>
                    <%--<td>Username</td>--%>
                    <%--<td><input type="text" name="userName"></td>--%>
                <%--</tr>--%>
                <%--<button id="updateEmployee" type="submit" class="btn btn-info btn-sm">Update</button>--%>
            <%--</form>--%>

            <!-- Modal -->
            <div id="myModel" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <form action="submitReimbursement.do" method="post">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Create a new Reimbursement</h4>
                            </div>

                            <div class="modal-body">
                                Type: <select name="rType">
                                <c:forEach var="temp" items="${listOfType}">
                                    <option value="${temp.typeID}">${temp.typeName}</option>
                                </c:forEach>
                            </select> <br/> Amount: <input type="number" step=".01" name="rAmount" /><br />
                                Description: <input type="text" name="rComments" /> <br />
                                Receipt: <input type="text" name="Receipt" /><br />
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>


