// var app = angular.module("ERS", []);
var app = angular.module("ERS", ['ui.router'])
    .config(function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("home");
        var loginState = {
                url: '/login',
                templateUrl: 'login.jsp'
            }
        var employeeState = {
                name:"employeeLogin",
                url: "/ers/login.do",
                templateUrl: '/ers/login.do'
            }

        $stateProvider.state(loginState);
        $stateProvider.state(employeeState);
    })

    app.controller("LoginController", function($scope, $http){
    // $scope.user = userName;
    // $scope.pass = passWord;
    $scope.checkLogin = function (u, p) {
        console.log("Checking login");
        $http({
            method: "POST",
            url: "/ers/login.do",
            data: {
                username: u,
                password: p
            }
        })
    }
    });
