package middleTier;

import domain.Employees;
import domain.ReimbType;
import domain.Reimbursement;
import domain.impl.Facade;

import javax.naming.AuthenticationException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Mitul on 6/18/2017.
 */
public class UserService {

    Employees authenticate(String username, String password) throws AuthenticationException, SQLException {
        Facade dataTier = new Facade();
        System.out.println(username+" @problem "+password);
        Employees user = dataTier.retrieveUserByUsername(username);
        String pass = user.getPassword();
        //String User = user.getUsername();

        System.out.println("\n"+user+ " @authenticate " +username+"\n");
        System.out.println("\n"+pass+ " @authenticate " +password+"\n");

        System.out.println(pass+ " " +password);

        if (user == null) {
            throw new AuthenticationException();
        }

        if(password.matches(pass)){

            return user;
        }
        else{
            throw new AuthenticationException();
        }
    }

    public List<Reimbursement> retrieveAllReimbursement() throws SQLException {
        return new Facade().retrieveAllReimbursement();
    }

    public List<Reimbursement> retrieveReimbursementsById(int userId) throws SQLException {
        return new Facade().retrieveReimbursementsByUserId(userId);
    }

    public void approveReimbursement(String[] idList, int resolverID, String status) throws SQLException {
        new Facade().approveReimbursements(idList, resolverID);
    }

    public void denyReimbursement(String[] idList, int resolverID) throws SQLException {
        new Facade().denyReimbursements(idList, resolverID);
    }

    public List<ReimbType> retrieveAllReimbType() throws SQLException {
        return new Facade().retrieveAllReimbType();
    }

    public void createNewReimbursement( Reimbursement reimb ) throws SQLException {
        new Facade().insert(reimb);
    }

    public void addEmployee(Employees emp) throws SQLException{
        new Facade().insertEmp(emp);
    }

    public List<Employees> retrieveAllEmp() throws SQLException {
        return new Facade().retrieveAllemps();
    }

    public void updateEmplopyee(int eid, String fName, String lName, String userName) throws SQLException {
        new Facade().update(eid, fName,lName, userName);
    }
}
