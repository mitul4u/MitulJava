package middleTier;

import domain.Employees;
import domain.Reimbursement;

import javax.naming.AuthenticationException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Mitul on 6/18/2017.
 */
public class BusinessDelegate {

    public Employees login(String user, String pass) throws AuthenticationException, SQLException {
        return new UserService().authenticate(user, pass);
    }

    public List<Reimbursement> getAllReimbursement() throws SQLException {
        return new UserService().retrieveAllReimbursement();
    }

    public List<Reimbursement> getReimbursementByUserId(int userId) throws SQLException {
        return new UserService().retrieveReimbursementsById(userId);
    }
}
