package domain;

public class Employees {
	private Integer employeeID;
	private Integer eRoles;
	private String fName;
	private String lName;
	private String password;
	private String username;

	public Employees(int userID, int roleID, String firstName, String lastName, String passWord, String userName) {
		super();
		this.employeeID = userID;
		this.username = userName;
		this.password = passWord;
		this.fName = firstName;
		this.lName = lastName;
		this.eRoles = roleID;
	}

    public Employees(int eRoles, String fName, String lName, String userName, String password) {
		this.eRoles = eRoles;
		this.fName = fName;
		this.lName = lName;
		this.username = userName;
		this.password = password;
    }

    public Integer getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(Integer employeeID) {
		this.employeeID = employeeID;
	}

	public Integer geteRoles() {
		return eRoles;
	}

	public void seteRoles(Integer eRoles) {
		this.eRoles = eRoles;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		return "Employees( userID:" + employeeID +
						" userName:" + username +
						" passWord:" + password +
						" firstName:" + fName +
						" lastName:" + lName +
						" roleID:" + eRoles;
	}
}
