package domain.impl;

import domain.ReimbType;
import utils.ConnectionUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by Mitul on 6/18/2017.
 */
public class ReimbTypeDAO {
		
	private Connection conn;
	
	public ReimbTypeDAO(Connection conn) {
		String dburl = "jdbc:oracle:thin:@mituldbinstance.c0iy8wuoxxld.us-east-1.rds.amazonaws.com:1521:ORCL";
		String dbusername = "mitul4u";
		String dbpassword = "R5g3fl5yb1!";
		try {
			conn = ConnectionUtil.newConnection(dburl,dbusername,dbpassword);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.conn = conn;
	}

	public List<ReimbType> getAllReimbType() throws SQLException {
		String sql = "select * from TYPE";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.executeQuery(sql);
		ResultSet rs = stmt.executeQuery();
		List<ReimbType> typeList = new ArrayList<ReimbType>();

		while (rs.next()) {
			ReimbType rType = new ReimbType( rs.getInt(1), rs.getString(2) );
			typeList.add( rType );
		}
		conn.close();
		return typeList;
	}
	
}
