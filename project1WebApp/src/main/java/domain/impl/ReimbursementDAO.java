package domain.impl;

import domain.Reimbursement;
import utils.ConnectionUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by Mitul on 6/18/2017.
 */
public class ReimbursementDAO {
	private Connection conn;
	public ReimbursementDAO(Connection conn) {
		String dburl = "jdbc:oracle:thin:@mituldbinstance.c0iy8wuoxxld.us-east-1.rds.amazonaws.com:1521:ORCL";
		String dbusername = "mitul4u";
		String dbpassword = "R5g3fl5yb1!";

		try {
			conn = ConnectionUtil.newConnection(dburl,dbusername,dbpassword);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.conn = conn;
	}

	public List<Reimbursement> getAllReimbursements() throws SQLException {
//		String sql = "SELECT REIMBERID," + " EID," + " MID," + " SUBMITTED," + " REIMBURAMOUNT," + " COMMENTS," +
//                "RECEIPT," + " RESOLVEDDATE," + " RESOLVEDAMT," + " STATUS," + " REIMBTYPE " +
//                "from Reimbursement";

		String sql= "SELECT re.REIMBERID, EID, MID, CONCAT(CONCAT(emp.FNAME, ' '), emp.LNAME) as Name," +
                    "CONCAT(CONCAT(e2.FNAME, ' '), e2.LNAME) as Manager," + "re.SUBMITTED," + "re.REIMBURAMOUNT,"+
				"re.COMMENTS," + "re.RECEIPT," + "re.RESOLVEDDATE," + "re.RESOLVEDAMT," + "stat.STATUS, stat.STATUSID, tp.TYPE " +
        "from reimbursement re JOIN status stat on re.STATUS = stat.STATUSID JOIN EMPLOYEE emp on emp.EMPLOYEEID = re.EID " +
        "JOIN type tp on tp.TYPEID = re.REIMBTYPE JOIN EMPLOYEE E2 ON E2.EMPLOYEEID = re.MID order by re.SUBMITTED";

		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.executeQuery(sql);
		ResultSet rs = stmt.executeQuery();

		List<Reimbursement> reimbList = new ArrayList<>();
		System.out.println("getAllReimbursement @ReimursementDao");

		while (rs.next()) {
			Reimbursement reImburse = new Reimbursement(rs.getInt(1), rs.getInt(2),rs.getInt(3),
					rs.getString(4), rs.getString(5), rs.getDate(6), rs.getDouble(7),
					rs.getString(8), rs.getString(9), rs.getDate(10), rs.getDouble(11),
					rs.getString(12), rs.getInt(13), rs.getString(14));
            reimbList.add(reImburse);
		}

		conn.close();
		return reimbList;
	}

	public List<Reimbursement> getReimbursementsByUserId(int userId) throws SQLException {
//		String sql = "SELECT re.REIMBERID," + " re.EID," + " re.MID," + " re.SUBMITTED," + " re.REIMBURAMOUNT," + " re.COMMENTS," +
//                "re.RECEIPT," + " re.RESOLVEDDATE," + " re.RESOLVEDAMT," + " re.STATUS," + " re.REIMBTYPE " +
//                "from Reimbursement re JOIN EMPLOYEE emp on re.EID = emp.EMPLOYEEID where eid = ? order by REIMBERID";

        String sql= "SELECT re.REIMBERID, re.EID, MID, CONCAT(CONCAT(emp.FNAME, ' '), emp.LNAME) as Name, " +
                "CONCAT(CONCAT(e2.FNAME, ' '), e2.LNAME) as Manager, re.SUBMITTED, re.REIMBURAMOUNT, re.COMMENTS, " +
                "re.RECEIPT, re.RESOLVEDDATE, re.RESOLVEDAMT, stat.STATUS, re.STATUS, tp.TYPE from reimbursement re " +
                "JOIN status stat on re.STATUS = stat.STATUSID JOIN type tp on tp.TYPEID = re.REIMBTYPE LEFT JOIN " +
                "EMPLOYEE E2 ON E2.EMPLOYEEID = re.MID JOIN EMPLOYEE emp on emp.EMPLOYEEID = re.EID where re.EID = ?";

		System.out.println("getReimbursementsByUserId @ReimursementDao");
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setInt(1, userId);
        System.out.println(userId);
        stmt.executeUpdate();
		ResultSet rs = stmt.executeQuery();

		List<Reimbursement> reimbList = new ArrayList<>();

		while (rs.next()) {
			Reimbursement reImburse = new Reimbursement(rs.getInt(1), rs.getInt(2),rs.getInt(3),
					rs.getString(4), rs.getString(5), rs.getDate(6), rs.getDouble(7),
					rs.getString(8), rs.getString(9), rs.getDate(10), rs.getDouble(11),
					rs.getString(12), rs.getInt(13),rs.getString(14));
			reimbList.add(reImburse);
		}
		conn.close();
		return reimbList;
	}

	public void changeReimbStatusToAccept(String[] idList, int resolverID) throws SQLException {
		try {
			Date now = new Date(System.currentTimeMillis());
			for (String id : idList) {
				String sql = "UPDATE REIMBURSEMENT SET STATUS = 2, MID = ?, RESOLVEDDATE =? Where REIMBERID = ? ";
				PreparedStatement stmt = conn.prepareStatement(sql);
				stmt.setInt(1, resolverID);
				stmt.setDate(2, now);
				stmt.setString(3, id);
				//stmt.setDouble(4, resolvedAmt);
				stmt.executeUpdate();
			}
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void changeReimbStatusToDenied(String[] idList, int resolverID) throws SQLException {
		try {
			Date now = new Date(System.currentTimeMillis());

			for (String id : idList) {
				String sql = "UPDATE REIMBURSEMENT SET STATUS = 3, MID = ?, RESOLVEDDATE =?  Where REIMBERID = ? ";
				PreparedStatement stmt = conn.prepareStatement(sql);
				stmt.setInt(1, resolverID);
				stmt.setDate(2, now);
				stmt.setString(3, id);
				stmt.executeUpdate();
			}
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void insert(Reimbursement reimb) throws SQLException {
		try {
			String sql = "INSERT INTO REIMBURSEMENT (reimberId, REIMBURAMOUNT, submitted, comments, eid, " +
					"status, reimbtype, RECEIPT) VALUES ((select max(reimberid)+1 from REIMBURSEMENT), ?, ?, ?, ?, ?,?, ?)";

			PreparedStatement stmt = conn.prepareStatement(sql);
			conn.setAutoCommit(false);

			//stmt.setInt(1, reimb.getReimberId());
			stmt.setDouble(1, reimb.getReimbursementAmount());
			stmt.setDate(2, reimb.getSubmitted());
			stmt.setString(3, reimb.getComments());
			stmt.setInt(4, reimb.geteId());
			stmt.setInt(5, reimb.getStatusId());
			stmt.setInt(6, reimb.getReimbTypeId());
			stmt.setString(7, reimb.getReceipt());

			stmt.executeUpdate();
			conn.commit();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
