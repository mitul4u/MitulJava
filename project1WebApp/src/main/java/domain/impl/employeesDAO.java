package domain.impl;

import domain.Employees;
import utils.ConnectionUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by Mitul on 6/18/2017.
 */
public class employeesDAO {

	private Connection conn;

	public employeesDAO(Connection conn) {
		super();
        String dburl = "jdbc:oracle:thin:@mituldbinstance.c0iy8wuoxxld.us-east-1.rds.amazonaws.com:1521:ORCL";
        String dbusername = "mitul4u";
        String dbpassword = "R5g3fl5yb1!";
        try {
            conn = ConnectionUtil.newConnection(dburl,dbusername,dbpassword);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        this.conn = conn;
	}

	public void insert(Employees user) throws SQLException {
		try {
			String sql = "INSERT INTO EMPLOYEE VALUES((SELECT MAX(EMPLOYEEID)+1 FROM EMPLOYEE), ?, ?, ?, ?, ?) ";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setInt(1, user.geteRoles());
			stmt.setString(2, user.getfName());
			stmt.setString(3, user.getlName());
			stmt.setString(4, user.getUsername());
			stmt.setString(5, user.getPassword());

			stmt.executeUpdate();
			//System.out.println("executed");
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<Employees> getAllUsers() throws SQLException {
		String sql = "select * from EMPLOYEE";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.executeQuery(sql);
		ResultSet rs = stmt.executeQuery();
		List<Employees> userList = new ArrayList<Employees>();

		System.out.println("getAllUsers@employeesDAO\n");
		while (rs.next()) {
			Employees user = new Employees(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5),
					 rs.getString(6));
			userList.add(user);
		}
		conn.close();
		return userList;
	}

	public Employees getUserByName(String username) throws SQLException {

		String sql = "select * from EMPLOYEE WHERE USERNAME = ?";
		PreparedStatement stmt = conn.prepareStatement(sql);

		//Testing
		System.out.println(username);

		stmt.setString(1, username);
		stmt.executeUpdate();
		ResultSet rs = stmt.executeQuery();

		Employees user = null;
		while (rs.next()) {
			user = new Employees(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5),
					 rs.getString(6));
		}
		//System.out.println("getUserByName@employeesDAO "+user.getUsername()+"\n");
		conn.close();
		return user;
	}

	public void updateEmployee(int eid, String fName, String lName, String userName) throws SQLException {
		try {
			String sql = "UPDATE EMPLOYEE SET fName = ?, lName = ? , userName = ? WHERE EMPLOYEEID = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);

			stmt.setString(1, fName);
			stmt.setString(2, lName);
			stmt.setString(3, userName);
			stmt.setInt(4, eid);
			stmt.executeUpdate();
			conn.close();
		} catch (SQLException e){
			e.printStackTrace();
		}
	}
}
