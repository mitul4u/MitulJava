package domain.impl;

import domain.Employees;
import domain.ReimbType;
import domain.Reimbursement;
import utils.ConnectionUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Project 1 webApp
 * Created by Mitul on 6/18/2017.
 */
public class Facade {

    private String url = "jdbc:oracle:thin:@mituldbinstance.c0iy8wuoxxld.us-east-1.rds.amazonaws.com:1521:ORCL";
    private String username = "mitul4u";
    private String password = "R5g3fl5yb1!";
    private Connection conn;

    public Facade() {
        try {
            conn = ConnectionUtil.newConnection(url,username,password);
            //System.out.println("Connection done successfully!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // insert one Reimbursement object into DB
    public void insert(Reimbursement reimb) throws SQLException {
        new ReimbursementDAO(conn).insert(reimb);
    }

    // insert one User object into DB
    public void insertEmp(Employees user) throws SQLException {
        new employeesDAO(conn).insert(user);
    }

    // retrieve Reimbursement data
    public List<Reimbursement> retrieveAllReimbursement() throws SQLException {
        return new ReimbursementDAO(conn).getAllReimbursements();
    }

//
//    // retrieve all users data
//    public List<Employees> retrieveAllUsers() throws SQLException {
//        return new employeesDAO(conn).getAllUsers();
//    }

    // retrieve user data where by username
    public Employees retrieveUserByUsername(String username) throws SQLException {
        return new employeesDAO(conn).getUserByName(username);
    }

    // retrieve reimbursements by user id
    public List<Reimbursement> retrieveReimbursementsByUserId(int userId) throws SQLException {
        return new ReimbursementDAO(conn).getReimbursementsByUserId(userId);
    }

    // approve the reimbursements status given list of reimbursement_id
    public void approveReimbursements(String[] idList, int resolverID) throws SQLException {
        new ReimbursementDAO(conn).changeReimbStatusToAccept(idList, resolverID);
    }

    // deny the reimbursements status given list of reimbursement_id
    public void denyReimbursements(String[] idList, int resolverID) throws SQLException {
        new ReimbursementDAO(conn).changeReimbStatusToDenied(idList, resolverID);
    }

    // retrieve all ReimbType data
    public List<ReimbType> retrieveAllReimbType() throws SQLException {
        return new ReimbTypeDAO(conn).getAllReimbType();
    }

    public List<Employees> retrieveAllemps() throws SQLException {
        return new employeesDAO(conn).getAllUsers();
    }

    public void update(int eid, String fName, String lName, String userName) throws SQLException {
        new employeesDAO(conn).updateEmployee(eid, fName, lName, userName);
    }
}
