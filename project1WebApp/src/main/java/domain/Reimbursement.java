package domain;

import java.sql.Date;

public class Reimbursement {

	private Integer reimberId;
	private Integer eId;
	private String empFullName;
    private String managerFullName;
	private Integer mId;
	private Date submitted;
	private Double reimbursementAmount;
	private String comments;
	private String receipt;
	private Date resolvedDate;
	private Double resolvedAmt;
	private String status;
	private String reimbType;
    private Integer reimbTypeId;
	private Integer statusId;

    public Integer getReimberId() {
		return reimberId;
	}

	public void setReimberId(Integer reimberId) {
		this.reimberId = reimberId;
	}

	public Integer geteId() {
		return eId;
	}

	public void seteId(Integer eId) {
		this.eId = eId;
	}

	public Integer getmId() {
		return mId;
	}

	public void setmId(Integer mId) {
		this.mId = mId;
	}

	public Date getSubmitted() {
		return submitted;
	}

	public void setSubmitted(Date submitted) {
		this.submitted = submitted;
	}

	public Double getReimbursementAmount() {
		return reimbursementAmount;
	}

	public void setReimbursementAmount(Double reimbursementAmount) {
		this.reimbursementAmount = reimbursementAmount;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getReceipt() {
		return receipt;
	}

	public void setReceipt(String receipt) {
		this.receipt = receipt;
	}

	public Date getResolvedDate() {
		return resolvedDate;
	}

	public void setResolvedDate(Date resolvedDate) {
		this.resolvedDate = resolvedDate;
	}

	public Double getResolvedAmt() {
		return resolvedAmt;
	}

	public void setResolvedAmt(Double resolvedAmt) {
		this.resolvedAmt = resolvedAmt;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReimbType() {
		return reimbType;
	}

	public void setReimbType(String reimbType) {
		this.reimbType = reimbType;
	}

    public String getEmpFullName() {
        return empFullName;
    }

    public void setEmpFullName(String empFullName) {
        this.empFullName = empFullName;
    }

    public String getManagerFullName() {
        return managerFullName;
    }

    public void setManagerFullName(String managerFullName) {
        this.managerFullName = managerFullName;
    }


    public Integer getReimbTypeId() {
        return reimbTypeId;
    }

    public void setReimbTypeId(Integer reimbTypeId) {
        this.reimbTypeId = reimbTypeId;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }
//	public Reimbursement(Integer reimberId, Integer eId, Integer mId, Date submitted, Integer reimbursementAmount,
//						 String comments, String receipt, Date resolvedDate, Integer resolvedAmt,
//						 Integer status, Integer reimbType) {
//		super();
//		this.reimberId = reimberId;
//		this.eId = eId;
//		this.mId = mId;
//		this.submitted = submitted;
//		this.reimbursementAmount = reimbursementAmount;
//		this.comments = comments;
//		this.receipt = receipt;
//		this.resolvedDate = resolvedDate;
//		this.resolvedAmt = resolvedAmt;
//		this.status = status;
//		this.reimbType = reimbType;
//	}

	public Reimbursement(Integer reimberId, Integer eId, Integer mId, String empFullName, String manaFullName, Date submitted, Double reimbursementAmount,
                         String comments, String receipt, Date resolvedDate, Double resolvedAmt,
                         String status, int statusId, String reimbType) {
		super();
		this.eId = eId;
		this.mId = mId;
		this.reimberId = reimberId;
		this.empFullName = empFullName;
		this.managerFullName = manaFullName;
		this.submitted = submitted;
		this.reimbursementAmount = reimbursementAmount;
		this.comments = comments;
		this.receipt = receipt;
		this.resolvedDate = resolvedDate;
		this.resolvedAmt = resolvedAmt;
		this.status = status;
		this.statusId = statusId;
		this.reimbType = reimbType;
	}

    public Reimbursement(double amount, Date timeSubmitted, String description, int authorID, int statusID,
						 int typeID, String reciept) {
        super();
        this.reimbursementAmount = amount;
        this.submitted = timeSubmitted;
        this.comments = description;
        this.eId = authorID;
        this.statusId = statusID;
        this.reimbTypeId = typeID;
        this.receipt = reciept;
    }
}
