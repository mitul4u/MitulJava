package domain;

/**
 * Created by Mitul on 6/18/2017.
 */
public class ReimbType {

    private int typeID;
    private String type;

    public ReimbType(int typeID, String typeName) {
        super();
        this.typeID = typeID;
        this.type = typeName;
    }

    public int getTypeID() {
        return typeID;
    }
    public void setTypeID(int typeID) {
        this.typeID = typeID;
    }
    public String getTypeName() {
        return type;
    }
    public void setTypeName(String typeName) {
        this.type = typeName;
    }

    @Override
    public String toString() {
        return "Types( typeID:" + typeID + " Name:" + type + ") ";
    }
}
