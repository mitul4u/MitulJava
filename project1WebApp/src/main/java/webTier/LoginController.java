package webTier;

import domain.Employees;
import domain.Reimbursement;
import middleTier.BusinessDelegate;

import javax.naming.AuthenticationException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Project 1 webApp
 * Created by Mitul on 6/18/2017.
 */

class LoginController {
    void login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //JSTL approach
        String user = request.getParameter("userid");
        String pass = request.getParameter("pass");
        //System.out.println("Login@LoginController "+user+" "+pass+"\n");

        //Angular Stuff
//        BufferedReader reader = request.getReader();
//        System.out.println(reader);
//
//        String s;
//        s = reader.readLine();
//        System.out.println(request+" request s "+s);
//        String str[] = s.split("\"");
//
//        String user = str[3];
//        String pass = str[7];
//        System.out.println("Login@LoginController "+user+" password "+pass+"\n");

        try {
            Employees session = new BusinessDelegate().login(user, pass);
            request.getSession().setAttribute("user", session);
            globalVariables g = new globalVariables();
            g.fullName = session.getfName() + " " + session.getlName();
            g.employeeSession = session.getEmployeeID();

            request.setAttribute("name", g.fullName);
            request.getSession().setMaxInactiveInterval(15*60);

            //System.out.println("LoginController: "+session);
            //System.out.println("ID @LoginController " +session.getUsername().toString()+" "+session.getPassword().toString()+"\n");

            if (session.geteRoles() == 2) {
                List<Reimbursement> list = new BusinessDelegate().getAllReimbursement();
                //System.out.println("Manager role: Fix me afterward @LoginController\n");
                request.setAttribute("REIMBList", list);
                //System.out.println("List @ LoginController: "+ list);
                request.getRequestDispatcher("manager.do").forward(request, response);
            } else {
                //System.out.println("Employee role: Fix me afterward @LoginController\n");
                int userId = session.getEmployeeID();
                //System.out.println(userId);
                List<Reimbursement> list = new BusinessDelegate().getReimbursementByUserId(userId);
                request.setAttribute("REIMBList", list);
                //System.out.println("List for employee @ LoginController: "+ list);
                request.getRequestDispatcher("employee.do").forward(request, response);
            }
        } catch (AuthenticationException e) {
            request.setAttribute("authFailed", "Incorrect password! ");
            request.getRequestDispatcher("login.jsp").forward(request, response);
        } catch (SQLException e) {
            request.setAttribute("authFailed", "Connection error! @LoginController");
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }
    }

    Employees validateSession(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if( request.getSession().getAttribute("user") != null ){
            return (Employees) request.getSession().getAttribute("user");
        }else{
            request.getRequestDispatcher("/error/403.html").forward(request, response);
            return null;
        }
    }
}
