package webTier;

import domain.Employees;
import domain.ReimbType;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
/**
 * Project 1 webApp
 * Created by Mitul on 6/18/2017.
 */
public class FrontControllerServlet extends HttpServlet {

    @Override
    public void init() throws ServletException {
        // overriding init() to use ServletContext
        try {
            List<ReimbType> listOfTypeObject = new MainController().getListOfReimbType();
            List<Employees> listEmpObject = new MainController().getListOfEmp();
            this.getServletContext().setAttribute("listOfType", listOfTypeObject);
            this.getServletContext().setAttribute("listOfEmp", listEmpObject);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String requestURI = request.getRequestURI();
        //System.out.println(requestURI);
        //System.out.println("Request @doPost "+request + " Response @doPost " + response +"\n");
        LoginController loginCtrl = new LoginController();

        switch (requestURI) {
            case "/ers/login.do": {
                try {
                    loginCtrl.login(request, response);
                    System.out.println("Request @doPost "+request + " Response @doPost " + response +"\n");
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            }
            case "/ers/employee.do": {
                loginCtrl.validateSession(request, response);
                System.out.println("Case employee.do @FrontControllerServlet");
                request.getRequestDispatcher("/secure/employeePortal.jsp").forward(request, response);
                break;
            }
            case "/ers/manager.do": {
                loginCtrl.validateSession(request, response);
                System.out.println("Case manager.do @FrontControllerServlet");
                request.getRequestDispatcher("/secure/managerPortal.jsp").forward(request, response);
                break;
            }
//            case "/ers/userPortal.do": {
//                System.out.println("Case manager.do @FrontControllerServlet");
//                request.getRequestDispatcher("/secure/userPortal.jsp").forward(request, response);
//                break;
//            }
            case "/ers/updateStatus.do": {
                System.out.println("Case updateStatus.do @FrontControllerServlet");
                try {
                    new MainController().updateReimbursementStatus(request, response);
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            }

            case "/ers/updateEmployee.do": {
                System.out.println("@updateEmployee.do");
                try {
                    new MainController().updateEmployee(request, response);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            }
            case "/ers/addEmployee.do": {
                System.out.println("@addEmployee.do");
                try {
                    new MainController().addEmployee(request, response);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            }
            case "/ers/submitReimbursement.do": {
                try {
                    new MainController().submitNewReimbursement(request, response);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            }
            case "/ers/logOut.do": {
                request.getSession().invalidate();
                request.getRequestDispatcher("login.jsp").forward(request, response);
                break;
            }
            default: {
                response.setStatus(404);
            }
        }
    }
}
