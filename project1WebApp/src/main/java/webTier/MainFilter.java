package webTier;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/**
 * Created by Mitul on 6/18/2017.
 */
public class MainFilter implements Filter {
    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
            throws IOException, ServletException {

        // logic to ensure a user has logged in
        HttpServletRequest request = (HttpServletRequest) req;
        if (request.getSession().getAttribute("user") != null) {
            //System.out.println("Authorized!");
            chain.doFilter(req, resp);
        } else {
            //System.out.println("Not authorized");
            HttpServletResponse response = (HttpServletResponse) resp;
            response.setStatus(403); // forbidden
        }
    }

    @Override
    public void destroy() {
    } // container shutdown

    @Override
    public void init(FilterConfig arg0) throws ServletException {
    }
}
