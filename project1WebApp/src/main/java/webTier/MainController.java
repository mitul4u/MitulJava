package webTier;

import domain.Employees;
import domain.ReimbType;
import domain.Reimbursement;
import middleTier.BusinessDelegate;
import middleTier.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;
/**
 * Created by Mitul on 6/18/2017.
 */
public class MainController {
    /**
     * Accept or Deny all the reimbursements that are checked
     */
    globalVariables g = new globalVariables();

    public void updateReimbursementStatus(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {

        String[] listOfIdToUpdate = request.getParameterValues("status");
        String status;
        status = request.getParameter("accept");
        //System.out.println(status);

        Employees session = (Employees) request.getSession().getAttribute("user");

        int resolverID = session.getEmployeeID();
        //double resolvedAmt = re.getResolvedAmt();

        if(status==null){
            status = "accept";
            new UserService().approveReimbursement(listOfIdToUpdate, resolverID, status);
        } else {
            new UserService().denyReimbursement(listOfIdToUpdate, resolverID);
        }


        // Re-fetch the data, then refresh the page
        List<Reimbursement> list = new BusinessDelegate().getAllReimbursement();
        List<Employees> empList = new UserService().retrieveAllEmp();

        request.setAttribute("name", g.fullName);
        request.setAttribute("REIMBList", list);
        request.setAttribute("listOfEmp", empList);
        request.getRequestDispatcher("/secure/managerPortal.jsp").forward(request, response);
    }

    public void updateEmployee(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
        String eid; //= request.getParameter("empid");

        if(request.getParameter("empid") == null){
            System.out.println(request.getParameter("empid"));
            eid = String.valueOf(g.employeeSession);
        } else{
            eid = request.getParameter("empid");
        }
        System.out.println(eid);

        //Employees session = (Employees) request.getSession().getAttribute("user");
        String fName = request.getParameter("fName");
        String lName = request.getParameter("lName");
        String userName = request.getParameter("userName");

        // Update employee
        new UserService().updateEmplopyee(Integer.parseInt(eid), fName,lName,userName);

        // Re-fetch the data, then refresh the page
        List<Reimbursement> list = new BusinessDelegate().getAllReimbursement();
        List<Employees> empList = new UserService().retrieveAllEmp();

        request.setAttribute("name", g.fullName);
        request.setAttribute("REIMBList", list);
        request.setAttribute("listOfEmp", empList);
        request.getRequestDispatcher("/secure/managerPortal.jsp").forward(request, response);
    }

    // returns a list of all Reimbursement types
    public List<ReimbType> getListOfReimbType() throws SQLException {
        return new UserService().retrieveAllReimbType();
    }

    // create a new Reimbursement record
    public void submitNewReimbursement(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {

        Employees session = (Employees) request.getSession().getAttribute("user");

        Double reimbAmount = Double.valueOf(request.getParameter("rAmount"));
        Date DateSubmitted = new java.sql.Date(System.currentTimeMillis());
        String reimbComments = request.getParameter("rComments");
        int reimbAuthorID = session.getEmployeeID();
        int reimbStatusID = 1;	// all new records has status pending (which has id = 1)
        int reimbTypeID = Integer.parseInt(request.getParameter("rType"));
        String reciept = request.getParameter("Receipt");

        Reimbursement newRec = new Reimbursement(reimbAmount, DateSubmitted, reimbComments, reimbAuthorID, reimbStatusID, reimbTypeID, reciept);
        new UserService().createNewReimbursement(newRec);

        // Re-fetch the data, then refresh the page
        List<Reimbursement> list = new BusinessDelegate().getReimbursementByUserId(session.getEmployeeID());
        request.setAttribute("name", g.fullName);
        request.setAttribute("REIMBList", list);
        request.getRequestDispatcher("/secure/employeePortal.jsp").forward(request, response);
    }

    public List<Employees> getListOfEmp() throws SQLException {
        return new UserService().retrieveAllEmp();
    }

    public void addEmployee(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
        int eRoles = Integer.parseInt(request.getParameter("eRoles"));
        String fName = request.getParameter("firstName");
        String lName = request.getParameter("lastName");
        String userName = request.getParameter("userName");
        String password = request.getParameter("password");
        Employees emp = new Employees(eRoles, fName, lName, userName, password);

        new UserService().addEmployee(emp);

        // Re-fetch the data, then refresh the page
        List<Reimbursement> list = new BusinessDelegate().getAllReimbursement();
        List<Employees> empList = new UserService().retrieveAllEmp();

        request.setAttribute("REIMBList", list);
        request.setAttribute("name", g.fullName);
        request.setAttribute("listOfEmp", empList);
        request.getRequestDispatcher("/secure/managerPortal.jsp").forward(request, response);
    }
}
